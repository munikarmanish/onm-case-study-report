TEX := pdflatex
MAIN := report
BIB := bibtex
WASTE := *.aux *.log *.toc *.out

######################

default: single

single:
	$(TEX) $(MAIN)

double:
	$(TEX) $(MAIN)
	$(TEX) $(MAIN)

all: clean
	$(TEX) $(MAIN)
	$(BIB) $(MAIN)
	$(TEX) $(MAIN)
	$(TEX) $(MAIN)

clean:
	rm -rf $(WASTE)
